from django.db import models

# Create your models here.

class Movie(models.Model):
    title = models.CharField(max_length=50)
    rating = models.IntegerField(default=1)
    description = models.TextField()
    release_date = models.DateTimeField('date released')

    def __str__(self):
        return self.title
